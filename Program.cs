﻿int CalculateGold(int orePieces)
{   
    // Lượng vàng nhận được từ mỗi quặng
    const int GOLD_PER_ORE = 10;
    
    // Tính tổng lượng vàng
    return orePieces * GOLD_PER_ORE; 
}
Console.OutputEncoding = System.Text.Encoding.UTF8;
// Lấy input từ người dùng
Console.Write("Nhập số lượng mảnh quặng: ");
int oreAmount = int.Parse(Console.ReadLine());
Console.OutputEncoding = System.Text.Encoding.UTF8;
// Tính và in ra số vàng nhận được
int goldAmount = CalculateGold(oreAmount);
Console.WriteLine("Bạn nhận được {0} đồng xu vàng", goldAmount);